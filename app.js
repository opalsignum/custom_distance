// libraries
const fs = require('fs');


//defintion of constants
const CUSTOMER_DATA_URL = "./data/customers.txt";
const CORRUPT_DATA = "./data/corrupt_data.txt";
const RADIUS = 100;
const HOMEBASE = {
	latitude: '52.493256',
	longitude: '13.446082'
}


/*
* Return the distance between one lat and long point and another
*
* @returns [array] - Data array
*/
var getData = (callback) => {

	var dataArray = [];

	try {
		var fileData = fs.readFileSync(CUSTOMER_DATA_URL).toString().split("\n");
		if (fileData !== undefined && fileData.length > 0) {
			for (var i = 0; i < fileData.length; i++) {
				//Remove traling comma
				var trim = fileData[i].replace(/,\s*$/, "");
				//Remove white space and split
				trim = trim.replace(/\s/g, '');
				var trim2 = trim.split(',');

				var cDataObject = toObject(trim2);
				dataArray.push(cDataObject);
			}

			callback(dataArray);
		}
	} catch (error) {
		console.log("File not found or not readable");
	}
}


/*
 * Helper function ro return the distance between one lat and long point and another
 * Great-Circle Distance algorithm
 * 
 * @returns {number} 
 */
var getCustomDistance = (lat1, lon1, lat2, lon2) => {
	var p = Math.PI / 180;
	var c = Math.cos;
	var a = 0.5 - c((lat2 - lat1) * p) / 2 +
		c(lat1 * p) * c(lat2 * p) *
		(1 - c((lon2 - lon1) * p)) / 2;

	return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}


/*
 * Sort result Ids and log them to the console
 */
var printCustomerIDs = (data) => {

	var resultIDs = [];
	for (var i = 0; i < data.length; i++) {
		var customerDistance = getCustomDistance(HOMEBASE.latitude, HOMEBASE.longitude, data[i].lat, data[i].long);
		if (customerDistance <= RADIUS) {
			resultIDs.push(data[i].id);
			//cconsole.log(customerDistance);
		}
	}

	var sortedIDs = resultIDs.sort();
	console.log(sortedIDs);
}


/*
 * Helper function to restructure data from array to json conform object
 * 
 * @param {array} arr 
 * @returns an object {}
 */
function toObject(arr) {
	var rv = {};
	if (arr !== undefined || arr.length > 0) {
		for (var i = 0; i < arr.length; ++i) {
			var split = arr[i].split(':');
			var type = split[0];
			var name = split[1];
			rv[type] = name;
		}
		return rv;
	}
}

module.exports = {
	getCustomDistance,
	toObject
}

getData(printCustomerIDs);
